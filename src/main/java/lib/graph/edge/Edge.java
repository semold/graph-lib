package lib.graph.edge;

import lib.graph.vertex.Vertex;

/**
 * @param <VT> vertex type.
 */
public interface Edge<VT extends Vertex<?>> {
    /**
     * @return Start vertex.
     */
    VT from();

    /**
     * @return End vertex.
     */
    VT to();

    /**
     * @return Edge with reverted vertexes.
     */
    Edge<VT> revert();
}
