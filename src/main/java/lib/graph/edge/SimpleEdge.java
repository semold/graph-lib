package lib.graph.edge;

import java.util.Objects;
import lib.graph.vertex.Vertex;

/**
 * Simple edge, which connect two vertexes.
 *
 * @param <VT> vertex type.
 */
public class SimpleEdge<VT extends Vertex<?>> implements Edge<VT> {
    /**
     * Start vertex.
     */
    private final VT from;

    /**
     * End vertex.
     */
    private final VT to;

    /**
     * @param from Start vertex.
     * @param to End vertex.
     */
    public SimpleEdge(VT from, VT to) {
        this.from = from;
        this.to = to;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VT from() {
        return from;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VT to() {
        return to;
    }

    @Override
    public Edge<VT> revert() {
        return new SimpleEdge<>(to, from);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        SimpleEdge<?> that = (SimpleEdge<?>) o;

        return from.equals(that.from) && to.equals(that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return from.value() + "->" + to.value();
    }
}
