package lib.graph.vertex;

/**
 * @param <V> Vertex value type.
 */
public interface Vertex<V> {
    /**
     * @return value.
     */
    V value();
}
