package lib.graph.vertex;

import java.util.Objects;

/**
 * Simple vertex, which contains any value of {@link V} type.
 *
 * @param <V> vertex value type.
 */
public class SimpleVertex<V> implements Vertex<V> {
    /**
     * Vertex value.
     */
    private final V value;

    /**
     * @param value vertex value.
     */
    public SimpleVertex(V value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V value() {
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        SimpleVertex<?> that = (SimpleVertex<?>) o;

        return value.equals(that.value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
