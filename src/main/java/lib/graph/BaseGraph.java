package lib.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lib.graph.edge.Edge;
import lib.graph.vertex.Vertex;

/**
 * @param <V> Vertex value type.
 * @param <VT> Vertex type.
 * @param <ET> Edge type.
 * @param <SELF> Self generic type, needed for correct method chaining.
 */
public abstract class BaseGraph<V, VT extends Vertex<V>, ET extends Edge<VT>, SELF extends BaseGraph<V, VT, ET, SELF>> {
    /**
     * If {@code true}, then graph will be directed, otherwise undirected.
     */
    private boolean isDirected = false;

    /**
     * Map with vertexes and edges.
     */
    private final Map<VT, List<ET>> vertexWithEdges = new HashMap<>();

    /**
     * @return {@code true} if graph is directed.
     */
    public boolean isDirected() {
        return isDirected;
    }

    /**
     * @param directed if {@code true} then graph will be directed, otherwise undirected.
     * @return {@code this} form method chaining.
     */
    public SELF directed(boolean directed) {
        if (!vertexWithEdges.isEmpty()) {
            throw new IllegalStateException("You can't change directed property for non empty graph");
        }

        this.isDirected = directed;

        return self();
    }

    /**
     * @param value value of the graph vertex.
     * @return {@code this} form method chaining.
     */
    public SELF addVertex(V value) {
        Objects.requireNonNull(value, "Vertex value can't be null");

        addVertexInternal(wrapToVertex(value));

        return self();
    }

    /**
     * @param from start vertex.
     * @param to end vertex.
     * @return {@code this} form method chaining.
     */
    public SELF addEdge(V from, V to) {
        Objects.requireNonNull(from, "Vertex value can't be null");
        Objects.requireNonNull(to, "Vertex value can't be null");

        addEdgeInternal(wrapToVertex(from), wrapToVertex(to), wrapToEdge(from, to));

        return self();
    }

    /**
     * @param from start vertex.
     * @param to end vertex.
     * @return list of edges between vertexes or {@link Collections#emptyList} if path doesn't exists.
     */
    public List<Edge<Vertex<V>>> getPath(V from, V to) {
        VT fromVertex = wrapToVertex(from);
        VT toVertex = wrapToVertex(to);

        assertThatVertexExists(fromVertex);
        assertThatVertexExists(toVertex);

        LinkedList<VT> queue = new LinkedList<>();
        Map<VT, ET> visitedVertexes = new HashMap<>();

        queue.add(fromVertex);

        while (!queue.isEmpty()) {
            VT current = queue.pop();

            for (ET edge : vertexWithEdges.get(current)) {
                if (visitedVertexes.containsKey(edge.to())) {
                    continue;
                }

                visitedVertexes.put(edge.to(), edge);

                if (edge.to().value().equals(to)) {
                    return restorePath(visitedVertexes, from, to);
                }

                queue.push(edge.to());
            }
        }

        return Collections.emptyList();
    }

    /**
     * @return {@code this} for correct method chaining.
     */
    protected abstract SELF self();

    /**
     * @param value value of the vertex.
     * @return {@link Vertex} instance.
     */
    protected abstract VT wrapToVertex(V value);

    /**
     * @param from start vertex.
     * @param to end vertex.
     * @return {@link Edge} instance.
     */
    protected abstract ET wrapToEdge(V from, V to);

    /**
     * @param vertex {@link Vertex} instance.
     * @throws IllegalArgumentException if vertex already exists.
     */
    protected void addVertexInternal(VT vertex) {
        if (vertexWithEdges.containsKey(vertex)) {
            throw new IllegalArgumentException("The vertex already exists in the graph");
        }

        vertexWithEdges.put(vertex, new ArrayList<>());
    }

    /**
     * @param from from vertex.
     * @param to end vertex.
     * @param edge edge.
     */
    protected void addEdgeInternal(VT from, VT to, ET edge) {
        assertThatVertexExists(from);
        assertThatVertexExists(to);

        if (isDirected()) {
            vertexWithEdges.get(from).add(edge);
        } else {
            vertexWithEdges.get(from).add(edge);
            // Oops, java generics are not so powerful as we want :(
            vertexWithEdges.get(to).add((ET) edge.revert());
        }
    }

    /**
     * @param visitedVertexes Map of visited vertexes.
     * @param from start vertex.
     * @param to end vertex.
     * @return list of edges.
     */
    private List<Edge<Vertex<V>>> restorePath(Map<VT, ET> visitedVertexes, V from, V to) {
        Vertex<V> cur = wrapToVertex(to);
        List<Edge<Vertex<V>>> result = new ArrayList<>();

        while (cur.value() != from) {
            // Unchecked cast again...
            Edge<Vertex<V>> edge = (Edge<Vertex<V>>) visitedVertexes.get(cur);

            result.add(edge);

            cur = edge.from();
        }

        Collections.reverse(result);

        return result;
    }

    /**
     * @param vertex vertex.
     * @throws IllegalArgumentException if vertex not found in graph.
     */
    private void assertThatVertexExists(VT vertex) {
        if (!vertexWithEdges.containsKey(vertex)) {
            throw new IllegalArgumentException("Vertex not found in graph");
        }
    }
}
