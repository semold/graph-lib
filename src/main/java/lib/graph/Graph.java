package lib.graph;

import lib.graph.edge.SimpleEdge;
import lib.graph.vertex.SimpleVertex;

/**
 * Simple graph, each vertex contains any user type of {@link V} and can be connected with {@link SimpleEdge}.
 */
public class Graph<V> extends BaseGraph<V, SimpleVertex<V>, SimpleEdge<SimpleVertex<V>>, Graph<V>> {
    /**
     * {@inheritDoc}
     */
    @Override
    protected Graph<V> self() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SimpleVertex<V> wrapToVertex(V value) {
        return new SimpleVertex<>(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SimpleEdge<SimpleVertex<V>> wrapToEdge(V from, V to) {
        return new SimpleEdge<>(wrapToVertex(from), wrapToVertex(to));
    }
}
