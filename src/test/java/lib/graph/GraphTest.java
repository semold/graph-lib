package lib.graph;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lib.graph.edge.Edge;
import lib.graph.edge.SimpleEdge;
import lib.graph.vertex.SimpleVertex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GraphTest {
    @Test
    void shouldFindPathInLinearGraph() {
        Graph<Integer> graph = new Graph<Integer>()
            .addVertex(1)
            .addVertex(2)
            .addVertex(3)
            .addVertex(4)
            .addEdge(1, 2)
            .addEdge(2, 3)
            .addEdge(3, 4);

        assertEquals(getPathByString("4->3 3->2 2->1"), graph.getPath(4, 1));
    }

    @Test
    void shouldFindPathInCaseWhenVertexHasMultipleEdges() {
        Graph<Integer> graph = new Graph<Integer>()
            .addVertex(1)
            .addVertex(2)
            .addVertex(3)
            .addVertex(4)
            .addEdge(1, 2)
            .addEdge(1, 4)
            .addEdge(3, 4);

        assertEquals(getPathByString("3->4 4->1 1->2"), graph.getPath(3, 2));
    }

    @Test
    void shouldFindPathInCaseWhenGraphContainsLoop() {
        Graph<Integer> graph = new Graph<Integer>()
            .addVertex(1)
            .addVertex(2)
            .addVertex(3)
            .addVertex(4)
            .addVertex(5)
            .addEdge(1, 2)
            .addEdge(2, 3)
            .addEdge(3, 4)
            .addEdge(3, 1)
            .addEdge(1, 5);

        assertEquals(getPathByString("4->3 3->1 1->5"), graph.getPath(4, 5));
    }

    @Test
    void shouldFindPathInLinearDirectedGraph() {
        Graph<Integer> graph = new Graph<Integer>()
            .directed(true)
            .addVertex(1)
            .addVertex(2)
            .addVertex(3)
            .addVertex(4)
            .addEdge(1, 2)
            .addEdge(2, 3)
            .addEdge(3, 4);

        assertEquals(getPathByString("1->2 2->3 3->4"), graph.getPath(1, 4));
    }

    @Test
    void shouldNotFindPathInLinearDirectedGraph() {
        Graph<Integer> graph = new Graph<Integer>()
            .directed(true)
            .addVertex(1)
            .addVertex(2)
            .addVertex(3)
            .addVertex(4)
            .addEdge(1, 2)
            .addEdge(2, 3)
            .addEdge(3, 4);

        assertEquals(Collections.emptyList(), graph.getPath(4, 1));
    }

    @Test
    void shouldThrowExceptionWhenTryToChangeDirectedPropertyOnNonEmptyGraph() {
        Graph<Integer> graph = new Graph<Integer>().addVertex(1);

        IllegalStateException ex = assertThrows(IllegalStateException.class, () -> graph.directed(true));

        assertEquals("You can't change directed property for non empty graph", ex.getMessage());
    }

    @Test
    void shouldThrowExceptionWhenTryToFindPathForNotExistsVertex() {
        Graph<Integer> graph = new Graph<Integer>().addVertex(1);

        assertThrows(IllegalArgumentException.class, () -> graph.addVertex(1));
    }

    @Test
    void shouldThrowExceptionIfTryToAddExistingVertex() {
        Graph<Integer> graph = new Graph<Integer>().addVertex(1);

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> graph.getPath(1, 3));

        assertEquals("Vertex not found in graph", ex.getMessage());

        ex = assertThrows(IllegalArgumentException.class, () -> graph.getPath(3, 1));

        assertEquals("Vertex not found in graph", ex.getMessage());
    }

    @Test
    void shouldThrowNPEIfTryToAddVertexWithNullValue() {
        Graph<Integer> graph = new Graph<>();

        NullPointerException ex = assertThrows(NullPointerException.class, () -> graph.addVertex(null));

        assertEquals("Vertex value can't be null", ex.getMessage());
    }

    @Test
    void shouldThrowNPEIfTryToAddEdgeWithNullVertex() {
        Graph<Integer> graph = new Graph<>();

        NullPointerException ex = assertThrows(NullPointerException.class, () -> graph.addEdge(null, 1));

        assertEquals("Vertex value can't be null", ex.getMessage());

        ex = assertThrows(NullPointerException.class, () -> graph.addEdge(1, null));

        assertEquals("Vertex value can't be null", ex.getMessage());

        ex = assertThrows(NullPointerException.class, () -> graph.addEdge(null, null));

        assertEquals("Vertex value can't be null", ex.getMessage());
    }

    @Test
    void shouldThrowExceptionIfTryToAddEdgeWithNotExistsVertex() {
        Graph<Integer> graph = new Graph<Integer>().addVertex(1);

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> graph.addEdge(2, 1));

        assertEquals("Vertex not found in graph", ex.getMessage());

        ex = assertThrows(IllegalArgumentException.class, () -> graph.addEdge(1, 2));

        assertEquals("Vertex not found in graph", ex.getMessage());
    }

    private List<Edge<SimpleVertex<Integer>>> getPathByString(String path) {
        return Arrays.stream(path.split(" "))
            .map(edge -> {
                String[] parts = edge.split("->");

                return new SimpleEdge<>(
                    new SimpleVertex<>(Integer.parseInt(parts[0])),
                    new SimpleVertex<>(Integer.parseInt(parts[1]))
                );
            })
            .collect(Collectors.toList());
    }
}
