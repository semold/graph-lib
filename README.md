### The simple graph lib

This lib support of simple directed and undirected graphs. 
The vertexes can be any type of and can be connected between other vertexes with edges.

### Example of usage

Create a simple undirected graph:

```java
Graph<Integer> graph = new Graph<Integer>()
    .addVertex(1)
    .addVertex(2)
    .addVertex(3)
    .addEdge(1, 2)
    .addEdge(2, 3)
```

Create a simple directed graph:

```java
Graph<Integer> graph = new Graph<Integer>()
    .directed(true)
    .addVertex(1)
    .addVertex(2)
    .addVertex(3)
    .addEdge(1, 2)
    .addEdge(2, 3)
```

You can find path between two vertexes with `getPath` method:

```java
Graph<Integer> graph = new Graph<Integer>()
    .addVertex(1)
    .addVertex(2)
    .addVertex(3)
    .addVertex(4)
    .addVertex(5)
    .addEdge(1, 2)
    .addEdge(2, 3)
    .addEdge(3, 4)
    .addEdge(3, 1)
    .addEdge(1, 5);

List<Edge<Vertex<Integer>>> path = graph.getPath(4, 5); // should be 4->3 3->1 1->5
```


